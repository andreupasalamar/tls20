# OpenSSL

*Andreu Pasalamar Carbó*

## Conceptes clau

1. Criptografía

	- Simètrica: secret compartit, claus curtes nº bits, algoritme ràpid.
	- Asimètrica: Clau pública i clau privada, llarg nº bits, algoritmes pesats.
	- Híbrida: Asimètrica per establiment i negociació de secret compartit, data on motion amb simètrica
	
1. Web of trust

	Cada usuari decideix amb qui confiar.
	
1. PKI, Public Key Infrastructure


## Certificats digitals / TLS

Un certificat conté la clau pública i la identitat del titular. La identitat té una format X509 (`cn=...,ou=...`).
 El més important d'aquest format és el cn.
 
EL certificat també conté informació de l'autoritat (*Issuer*).

Trust / Validesa de qui firma ?
 Tenim una estructura piramidal de CA, amb múltiples arrels.
 
### TLS

Son tots aquells protocols que utilitzen TLS/SSL.

- Servidor: certificat de servidor.
- Client: certificat de client.


## Practiquem claus privades

Necessitem tenir instal·lat OpenSSL.

`[root@fedora ~]# dnf -y install openssl`

Subcomandes:

```text
Standard commands
asn1parse         ca                ciphers           cms               
crl               crl2pkcs7         dgst              dhparam           
dsa               dsaparam          ec                ecparam           
enc               engine            errstr            gendsa            
genpkey           genrsa            help              list              
nseq              ocsp              passwd            pkcs12            
pkcs7             pkcs8             pkey              pkeyparam         
pkeyutl           prime             rand              rehash            
req               rsa               rsautl            s_client          
s_server          s_time            sess_id           smime             
speed             spkac             srp               storeutl          
ts                verify            version           x509              

Message Digest commands (see the `dgst' command for more details)
blake2b512        blake2s256        gost              md2               
md4               md5               rmd160            sha1              
sha224            sha256            sha3-224          sha3-256          
sha3-384          sha3-512          sha384            sha512            
sha512-224        sha512-256        shake128          shake256          
sm3               

Cipher commands (see the `enc' command for more details)
aes-128-cbc       aes-128-ecb       aes-192-cbc       aes-192-ecb       
aes-256-cbc       aes-256-ecb       aria-128-cbc      aria-128-cfb      
aria-128-cfb1     aria-128-cfb8     aria-128-ctr      aria-128-ecb      
aria-128-ofb      aria-192-cbc      aria-192-cfb      aria-192-cfb1     
aria-192-cfb8     aria-192-ctr      aria-192-ecb      aria-192-ofb      
aria-256-cbc      aria-256-cfb      aria-256-cfb1     aria-256-cfb8     
aria-256-ctr      aria-256-ecb      aria-256-ofb      base64            
bf                bf-cbc            bf-cfb            bf-ecb            
bf-ofb            camellia-128-cbc  camellia-128-ecb  camellia-192-cbc  
camellia-192-ecb  camellia-256-cbc  camellia-256-ecb  cast              
cast-cbc          cast5-cbc         cast5-cfb         cast5-ecb         
cast5-ofb         des               des-cbc           des-cfb           
des-ecb           des-ede           des-ede-cbc       des-ede-cfb       
des-ede-ofb       des-ede3          des-ede3-cbc      des-ede3-cfb      
des-ede3-ofb      des-ofb           des3              desx              
idea              idea-cbc          idea-cfb          idea-ecb          
idea-ofb          rc2               rc2-40-cbc        rc2-64-cbc        
rc2-cbc           rc2-cfb           rc2-ecb           rc2-ofb           
rc4               rc4-40            rc5               rc5-cbc           
rc5-cfb           rc5-ecb           rc5-ofb           seed              
seed-cbc          seed-cfb          seed-ecb          seed-ofb          
zlib              
```

Generar una clau privada RSA de 2048 bits amb des3. Haurem d'introduïr pasphrase:

`$ openssl genrsa -des3 -out private.key 2048`

Els formats de les claus privades pot ser:

- Base64(PEM)
- Binari(DER)

Llavors haurem de posar la extensió que toca.

Des3 és un algoritme simètric amb una *passphrase* per protegir la clau privada. El fitxer no conté la clau sinó la
 clau xifrada. Per usar la clau cada vegada haurem de desblocarla amb la *passphrase*

```text
-----BEGIN RSA PRIVATE KEY-----
Proc-Type: 4,ENCRYPTED
DEK-Info: DES-EDE3-CBC,3F27AE42B57A5B3E
```

Generar clau privada RSA de 4096 bits.

`$ openssl genrsa -out private2.pem 4096`

Per treballar amb claus privades posem la subcomanda `rsa`.

Mostrar components sense tornar a fer volcat d'una clau rsa:

`$ openssl rsa -noout -text -in privatekey.pem`

Contingut:

- moduls
- exponent públic
- exponent privat
- primer nombre primer
- segon nombre primer
- primer exponent
- segon exponent
- coeficient

Si no posem `-noout` ens torna a mostrar la clau a banda de tota la informació.

Transformem `privada2.pem` (sense passphrase) per tenir passphrase:

`$ openssl rsa -des3 -in privada2.pem -out privada3.pem`

Transformem `privatekey.pem` (amb passphrase) per no tenir passphrase:

`$ openssl rsa -in privatekey.pem -out privada4.pem`

Generar clau pública a partir d'una clau privada.

`$ openssl rsa -in privada2.pem -pubout -out public.pem`

Transformar clau privada PEM a clau privada DER (binari):

`$ openssl rsa -inform PEM -in private.pem -outform DER -out private.der`


## Practiquem certificats:

- Autosignats
- Els que firma una CA

### Certificats autosignats

Hi han diverses estratègies per crear un certificat autosignat. La ordre per generar un certificat autosignat és:

`$ openssl req -new -x509 -nodes -out servercert.pem -keyout serverkey.pem`

On:
- req: *request*, demanar un certificat
- new: que sigui nou
- x509: certificat
- nodes: sense passphrase.
- out: el certificat que ens generarà (.pem)
- keyout: la clau privada que ens generarà (.pem)

Procès de creació (ojo amb els accents):

```text
Generating a RSA private key
.................................+++++
...................+++++
writing new private key to 'serverkey.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:ES
State or Province Name (full name) []:Barcelona
Locality Name (eg, city) [Default City]:Barcelona
Organization Name (eg, company) [Default Company Ltd]:Escola Del Treball
Organizational Unit Name (eg, section) []:Informàtica
Common Name (eg, your name or your server's hostname) []:web.edt.org
Email Address []:admin@edt.org
```
**IMPORTANT** que el `cn` sigui el que toca.

Veure el contingut:

`$ openssl x509 -noout -text -in servercert.pem`

Contingut:

- Versió
- Issuer
- Validesa (per defecte 1 mes)
- Titular
- Moduls
- Extensions
  - `Basic Constraits: critical CA:TRUE`: Amb aquest certificat ne puc generar de nous, ja que és un certificat
   d'autoritat. Tots els certificats autogenerats serveixen per actuar de `CA`.
- Signatura del *Issuer*

Mostrar l'emissor:

`$ openssl x509 -noout -issuer -in servercert.pem`

Mostrar el titular:

`$ openssl x509 -noout -subject -in servercert.pem`

Mostrar data d'inici i final:

`$ openssl x509 -noout -startdate -enddate -in servercert.pem`

Mostrar per a qué serveix el certificat:

`$ openssl x509 -noout -purpose -in servercert.pem`

Mostrar email i hash del certificat:

`$ openssl x509 -noout -email -hash -in servercert.pem`

Fer certificat a partir d'una clau

`$ openssl req -new -x509 -key key.pem -out cert.pem`

Quan li diem `-keyout` és per crear una nova clau i quan posem `-key` agafa una ja existent.
 Amb `-key` no cal `-nodes`.

Extreure clau pública de clau privada

`$ openssl rsa -pubout -in key.pem -out pub.pem`

Certificat vàlid per 365 dies

`$openssl req -new -x509 -key private.key -out cert.pem -days 365`

Certificat per 10 anys i amb sha512

`$ openssl req -new -x509 -key private.pem -out cert.pem -days $((365*10)) -sha512`

Crear nou certificat creant nova clau rsa:1024 sense passphrase

`$ openssl req -new -x509 -nodes -newkey rsa:1024 -keyout key.pem -out cert.pem`

Crear nou certificat especificant el *subject*.

`$ openssl req -new -x509 -nodes -newkey rsa:1024 -keyout key.pem -out cert.pem -subj '/C=es/ST=cs/L=Vinaros/CN=andreu/'`

Preguntar per el issuer i subject a la clau anterior

`$ openssl x509 -noout -issuer -subject -in cert.pem`


### Certificats d'autoritat

Creació d'un certificat d'autoritat

```text
$ openssl req -new -x509 -nodes -keyout cakey.pem -out cacert.pem
Generating a RSA private key
..........................+++++
................................................................+++++
writing new private key to 'cakey.pem'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:CA
State or Province Name (full name) []:Catalunya
Locality Name (eg, city) [Default City]:Barcelona
Organization Name (eg, company) [Default Company Ltd]:Veritat Absoluta CA
Organizational Unit Name (eg, section) []:Certificats
Common Name (eg, your name or your server's hostname) []:Veritat Absoluta
Email Address []:adm@edt.org
```

Per tenir un certificat avalat per la CA necessitem:

- Tenir la clau privada. Li sumará pubkey + ID subject + dades per formar un request.

Generar el **request** (petició de certificat).

`$ openssl req -new -key serverkey.pem -out serverreq.pem`

La CA agafará la petició i verifica la identitat. Signará amb la seva clau privada i posarà les claus del
 *issuer*. Un cop ho tingui fet ho enviarà al client.

```text
$ openssl x509 -CAkey cakey.pem -CA cacert.pem -req -in serverreq.pem -out servercert.pem -CAcreateserial
Signature ok
subject=C = CA, ST = Catalunya, L = Barcelona, O = escola del treball, OU = informatica, CN = web.edt.org, emailAddress = adm@edt.org
Getting CA Private Key
```

Comprovar qui és el *issuer* i el *subject*:

```text
$ openssl x509 -noout -issuer -subject -in servercert.pem 
issuer=C = CA, ST = Catalunya, L = Barcelona, O = Veritat Absoluta CA, OU = Certificats, CN = Veritat Absoluta, emailAddress = adm@edt.org
subject=C = CA, ST = Catalunya, L = Barcelona, O = escola del treball, OU = informatica, CN = web.edt.org, emailAddress = adm@edt.org
```

### Conexió amb altres hosts. Exemples d'ordres

`openssl s_client -connect 172.17.0.2:443`

`openssl s_client -connect www.web1.org:443`

`openssl s_client -servername www.web2.org -connext 172.17.0.2:443`

`openssl s_client -connext www.exemple.com:443 < /dev/null 2> /dev/null | openssl x509 -noout -text`

`openssl s_server -key key.pem -cert cert.pem -accept 55555 -www`

`openssl verify servercert.pem`

`openssl verify -CAfile cacert.pem servercert.pem`

Curl:

`curl -v -ssl https://www.exemple.com`

`curl -v -ssl --cacert cacert.pem https://www.exemple.com`

Ncat:

`ncat -l -ssl -k 55555`

`ncat -ssl localhost 55555`


### Definit el nostre propi fitxer de configuració

El fitxer de configuració és:

`/etc/pki/tls/openssl.cnf`

I per agafar un fitxer diferent al de per defecte:

`openssl req -new -x509 -key key.pem -out auto.pem -config openssl.cnf`











